package com.denis_kurmashev;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.io.*;
import java.sql.*;
import java.util.Properties;

import javax.servlet.*;
import javax.servlet.http.*;
import oracle.jdbc.pool.OracleDataSource;

public class DB {

  protected String serverName = "";
  protected String dbName = "";
  protected int port = 0;
  protected String user = "";
  protected String password = "";

  protected OracleDataSource dataSource = null;
  protected Connection connect = null;

  private String createTable() {
    String query = "CREATE TABLE _DENIS_users(" + "id INT," + "name VARCHAR(255) NOT NULL,"
        + "password VARCHAR(255) NOT NULL," + "PRIMARY KEY (id)" + ")";

    try {
      PreparedStatement stm = this.connect.prepareStatement(query);
      stm.executeQuery();
    } catch (Exception e) {
      String error = "createTable: 34 -- " + e.getMessage();
      return error;
    }

    return null;
  }

  public DB(String serverName, String dbName, int port, String user, String password) {
    this.serverName = serverName;
    this.dbName = dbName;
    this.port = port;
    this.user = user;
    this.password = password;
  }

  public String installConnection() {
    try {
      this.dataSource = new OracleDataSource();
    } catch (Exception e) {
      String error = "installConnection: 53 -- " + e.getMessage();
      return error;
    }

    this.dataSource.setDriverType("thin");
    this.dataSource.setServerName(this.serverName);
    this.dataSource.setDatabaseName(this.dbName);
    this.dataSource.setUser(this.user);
    this.dataSource.setPassword(this.password);
    this.dataSource.setPortNumber(this.port);

    try {
      this.connect = this.dataSource.getConnection();

    } catch (Exception e) {
      String error = "installConnection: 72 -- " + e.getMessage();
      return error;
    }

    String tableCreation = this.createTable();

    if (tableCreation != null) {
      String error = "Could not create table _DENIS_users -- ";
      return error + tableCreation;
    }

    return null;
  }

  public String addUser(String id) {
    String query = "INSERT INTO _denis_users(id, name, password) VALUES(" + id + ", 'Denis', '1111')";

    try {
      PreparedStatement stm = this.connect.prepareStatement(query);
      stm.executeQuery();
    } catch (Exception e) {
      String error = "addUser: 91 -- " + e.getMessage();
      return error;
    }

    return null;
  }

  public String getAllData() {
    String query = "SELECT * FROM _denis_users";
    String result = "<table>";

    try {
      PreparedStatement stm = this.connect.prepareStatement(query);
      ResultSet rss = stm.executeQuery();

      while (rss.next()) {
        result += "<tr>" + "<td>" + rss.getString(0) + "</td>" + "<td>" + rss.getString(1) + "</td>" + "<td>"
            + rss.getString(2) + "</td>" + "</tr>";
      }
      result += "</table>";

    } catch (Exception ex) {
      return "";
    }

    return result;
  }

  public String close() {
    try {
      this.connect.close();
    } catch (Exception e) {
      String error = "close: 91 -- " + e.getMessage();
      return error;
    }

    return null;
  }

}
