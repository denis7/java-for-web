package com.denis_kurmashev;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class Index extends HttpServlet {

  private static final long serialVersionUID = 1L;

  protected DB db = null;
  protected String error = null;

  @Override
  public void init() throws ServletException {
    super.init();

    ServletContext sc = getServletContext();

    this.db = new DB(sc.getInitParameter("serverName"), sc.getInitParameter("dbName"),
        Integer.parseInt(sc.getInitParameter("port")), sc.getInitParameter("user"), sc.getInitParameter("password"));
    this.error = db.installConnection();
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String id = request.getParameter("id");
    this.error = this.db.addUser(id);

    if (this.error != null) {
      response.getWriter().println(this.error);
      return;
    }

    response.getWriter().println("added");
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if (this.error != null) {
      response.getWriter().println(this.error);
      return;
    }

    response.getWriter().println(this.db.getAllData());
  }

  @Override
  public void destroy() {
    super.destroy();

    this.db.close();
  }
}
