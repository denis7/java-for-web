package com.denis_kurmashev;

import java.io.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

public class FileProvider extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
		String fileName = rq.getParameter("file");
		String rootDir = getServletContext().getInitParameter("doc-dir");

		File dir = new File(rootDir + File.separator + fileName);

		OutputDoc(dir, rs);
	}

	protected void OutputDoc(File doc, HttpServletResponse rs) throws IOException {
		rs.setContentType("application/msword");
		rs.addHeader("Content-Disposition", "attachment; filename=" + doc.getName());
		rs.setContentLength((int) doc.length());

		FileInputStream in = new FileInputStream(doc);
		BufferedInputStream buf = new BufferedInputStream(in);
		ServletOutputStream out = rs.getOutputStream();
		int readBytes = 0;

		while ((readBytes = buf.read()) != -1) {
			out.write(readBytes);
		}

		System.out.println("Downloading is launching. \nFile name: " + doc.getName());
	}

}