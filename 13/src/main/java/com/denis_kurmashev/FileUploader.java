package com.denis_kurmashev;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "FileUploader", urlPatterns = { "/upload" })
@MultipartConfig
public class FileUploader extends HttpServlet {

  private static final long serialVersionUID = 1L;

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");

    OutputStream out = null;
    InputStream fileContent = null;
    Part filePart = request.getPart("file");
    String fileName = getFileName(filePart);
    PrintWriter writer = response.getWriter();

    String serverPath = getServletContext().getInitParameter("doc-dir");

    try {
      out = new FileOutputStream(new File(serverPath + File.separator + fileName));
      fileContent = filePart.getInputStream();

      int read = 0;
      byte[] bytes = new byte[1024];

      while ((read = fileContent.read(bytes)) != -1) {
        out.write(bytes, 0, read);
      }

      writer.println("New file has been writing at path: " + serverPath);

    } catch (FileNotFoundException fne) {

      writer.println("Missing file or no insufficient permissions.");
      writer.println(" ERROR: " + fne.getMessage());

    } finally {

      if (out != null) {
        out.close();
      }

      if (fileContent != null) {
        fileContent.close();
      }

      if (writer != null) {
        writer.close();
      }

    }
  }

  private String getFileName(Part filePart) {

    String header = filePart.getHeader("content-disposition");
    String name = header.substring(header.indexOf("filename=\"") + 10);

    return name.substring(0, name.indexOf("\""));

  }
}