<%@ page contentType="text/html;charset=UTF-8" language="java"
         isELIgnored="false" %>
<html>
<head>
    <title>FORM</title>

    <style>

        .container {
            width: 50%;
            margin: 25%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            background: red;
        }

        .row {
            width: 90%;
            background: blue;
            padding: 5px;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: nowrap;
        }

        .row-item {
            width: 50%;
            color: #fAFAFA;
            text-align: center;
        }

    </style>

</head>
<body>

<%--<jsp:forward page="introduce-page.jsp"></jsp:forward>--%>

<% int hours = (new java.util.Date()).getHours(); %>

<% if (hours < 9) { %>
<jsp:include page="morning.jsp"></jsp:include>
<% } else if (hours < 17) { %>
<jsp:include page="evening.jsp"></jsp:include>
<% } else { %>
<jsp:include page="night.jsp"></jsp:include>
<% } %>

<div class="container">
    <% int increment = 60 * 60 * 24 * 1000; %>
    <% java.text.DateFormat df = new java.text.SimpleDateFormat("dd.mm.yyyy"); %>
    <% java.text.DateFormat weekFormater = new java.text.SimpleDateFormat("EEEE"); %>

    <% for (int i = 0; i < 7; i++) { %>

    <% java.util.Date date = new java.util.Date((new java.util.Date()).getTime() + (i * increment)); %>

    <div class="row">
        <div class="row-item"><%= df.format(date) %>
        </div>
        <div class="row-item"><%= weekFormater.format(date) %>
        </div>
    </div>

    <% } %>
</div>

</body>
</html>
