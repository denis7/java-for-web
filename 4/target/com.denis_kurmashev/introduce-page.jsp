<%@ page contentType="text/html;charset=UTF-8" language="java"
isELIgnored="false" %>
<html>
  <body>
    <%-- Deactivating Expression Evaluation --%>

    <h1>Example of servlet</h1>

    <h2>My name is ${name} ${surname}</h2>

    <h2>
      My name is <%= pageContext.findAttribute("name") %> <%=
      pageContext.findAttribute("surname") %>
    </h2>

    <% for (int i = 0; i < 5; i++) { %>

    <p><%= i%></p>

    <% } %>
  </body>
</html>
