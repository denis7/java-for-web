package com.denis_kurmashev;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import java.io.IOException;

public class DenisTag extends TagSupport {
   // For serealizible
   private static final long serialVersionUID = 1L;

   static String in = "<div class=\"name\">";
   public String userName = "";

   public String getValue() {
      return userName;
   }

   public void setValue(String userName) {
      this.userName = userName;
   }

   public int doStartTag() throws JspException {
      JspWriter out = pageContext.getOut();

      try {
         out.print(in + this.userName.toString());
      } catch (IOException e) {
         System.out.println("stafftag.Surname: " + e);
      }

      return SKIP_BODY;
   }

   public int doEndTag() throws JspException {
      String in = "</div>";
      JspWriter out = pageContext.getOut();

      try {
         out.print(in);
      } catch (IOException e) {
         System.out.println("stafftag.Dossier: " + e);
      }

      return EVAL_BODY_INCLUDE;
   }

}