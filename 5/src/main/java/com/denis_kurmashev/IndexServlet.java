package com.denis_kurmashev;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "indexServlet")
public class IndexServlet extends HttpServlet {
    private final Object AUTHOR_NAME = "DENIS";
    private final Object AUTHOR_SURNAME = "KURMASHEV";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");

        Object _name = this.AUTHOR_NAME, _surname = this.AUTHOR_SURNAME;

        if (name != null) {
            _name = name;
        }

        if (surname != null) {
            _surname = surname;
        }

        request.setAttribute("name", _name);
        request.setAttribute("surname", _surname);

        request.getRequestDispatcher("introduce-page.jsp").forward(request, response);
    }
}
