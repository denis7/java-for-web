package com.denis_kurmashev;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import java.io.IOException;

public class KurmashevTag extends TagSupport {
  // For serealizible
  private static final long serialVersionUID = 1L;

  public Integer age = 19;

  public Integer getAge() {
    return this.age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public int doStartTag() throws JspException {
    String in = "<div class=\"age\">" + this.age.toString();
    JspWriter out = pageContext.getOut();

    try {
      out.print(in);
    } catch (IOException e) {
      System.out.println("stafftag.Dossier: " + e);
    }

    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag() throws JspException {
    String in = "</div>";
    JspWriter out = pageContext.getOut();

    try {
      out.print(in);
    } catch (IOException e) {
      System.out.println("stafftag.Dossier: " + e);
    }

    return EVAL_BODY_INCLUDE;
  }

}