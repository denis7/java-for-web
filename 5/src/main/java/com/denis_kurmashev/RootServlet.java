package com.denis_kurmashev;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RootServlet")
public class RootServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String _name = request.getParameter("name"), _surname = request.getParameter("surname");

        String redirectUrl = "/introduce-page?name=" + _name + "&surname=" + _surname;

        response.sendRedirect(request.getContextPath() + redirectUrl);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("root.jsp").forward(request, response);
    }

}
