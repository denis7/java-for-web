<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %> 
<%@ taglib uri="./WEB-INF/_denis-kurmashev-tag-lib.tld" prefix="developer" %>
<html>  
  <head>
    <title>DENIS KURMASHEV APP</title>

    <style>
      .name {
        font-size: 25px;
        color: green;
      }

      .age { 
        font-size: 20px;
        color: red;
      }
    </style>

  </head>
  <body>
  
    <developer:denis value="Denis"></developer:denis>
    <developer:kurmashev age="19">
      <div style="color: blue"><%= 20 %></div>
    </developer:kurmashev>
  
  </body>
</html>
