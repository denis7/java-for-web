package com.denis_kurmashev;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetAllUrls extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  public void init() throws ServletException {
    super.init();

    ServletContext sc = getServletContext();

    sc.setAttribute("string_attr", "Denis Kurmashev");
    sc.setAttribute("int_attr", 19);
    sc.setAttribute("class_attr", new User("Denis", 19));

  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.getRequestDispatcher("get-all-items.jsp").forward(request, response);
  }
}
