package com.denis_kurmashev;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetAttribute extends HttpServlet {

  private static final long serialVersionUID = 1L;
  protected String stringAttr = "";
  protected int intAttr = 0;
  protected User classAttr = null;

  @Override
  public void init() throws ServletException {
    super.init();

    ServletContext sc = getServletContext();

    this.stringAttr = (String) sc.getAttribute("string_attr");
    this.intAttr = (Integer) sc.getAttribute("int_attr");
    this.classAttr = (User) sc.getAttribute("class_attr");

  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String l1 = "stringAttr = " + this.stringAttr + "\n";
    String l2 = "intAttr = " + this.intAttr + "\n";
    String l3 = "classAttr = " + this.classAttr.toString() + "\n";

    response.getWriter().println(l1 + l2 + l3);
  }
}
