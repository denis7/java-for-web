package com.denis_kurmashev;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.apache.http.impl.client.HttpClients;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import java.io.*;

public class Index extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected String makeRequest(String url) {
        if (url == null) {
            return "There is no url provided";
        }

        HttpClient httpClient = HttpClients.createDefault();
        HttpGet get = new HttpGet(url);

        try {
            HttpResponse response = httpClient.execute(get);
            int internResponseStatus = response.getStatusLine().getStatusCode();

            if (200 == internResponseStatus) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";

                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                return result.toString();

            } else {
                return "NULL";
            }
        } catch (Exception ex) {
            return "Executiong request error";
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url_id = request.getParameter("url_id");

        if (url_id == null) {
            response.getWriter().println("parameter URLn not found");
            return;
        }

        ServletContext sc = getServletContext();
        String uri = sc.getInitParameter("URL_" + url_id);

        String res = this.makeRequest(uri);

        response.setContentType("text/json");
        response.getWriter().println(res);

    }
}
