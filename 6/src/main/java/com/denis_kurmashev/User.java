package com.denis_kurmashev;

public class User {
  protected String name = "";
  protected int age = 0;

  public User(String name, int age) {
    this.name = name;
    this.age = age;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getAge() {
    return this.age;
  }

  public String toString() {
    return "name = " + this.name + "; \n age = " + this.age;
  }

}
