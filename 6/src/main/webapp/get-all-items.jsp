<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %> 
<html>  
  <head>
    <title>Denis Kurmashev Web Application</title>
  </head>
  <body>
  
  <%
    ServletContext sc = getServletContext();
    java.util.Enumeration en = sc.getInitParameterNames();
    String x;

    while (en.hasMoreElements()) {
      x = (String) en.nextElement(); %>
      
      <%= x + " = " %><%= sc.getInitParameter(x) %>
      <br />

    <% } %>
  
  </body>
</html>
