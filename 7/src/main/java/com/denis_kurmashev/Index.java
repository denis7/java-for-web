package com.denis_kurmashev;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

public class Index extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String sessiondId = session.getId();

        if (session.getAttribute(sessiondId) == null) {
            response.getWriter().write("You are already logged off!");
        } else {
            session.removeAttribute(sessiondId);
            response.getWriter().write("Successfully logged off!");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String sessiondId = session.getId();

        String name = request.getParameter("name");
        String age = request.getParameter("age");

        if (name == null || age == null) {
            response.getWriter().write("Data not provided!");
            return;
        }

        if (session.getAttribute(sessiondId) == null) {
            User user = new User(name, age);
            session.setAttribute(sessiondId, user);

            response.getWriter().write("Successfully auththorized!");
            return;
        } else {
            response.getWriter().write("You are already authenticated!");
            return;
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String sessiondId = session.getId();

        // if user not auth
        // render auth form
        // otherwise show profile page
        if (session.getAttribute(sessiondId) == null) {
            request.getRequestDispatcher("/auth.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/profile.jsp").forward(request, response);
        }

    }
}
