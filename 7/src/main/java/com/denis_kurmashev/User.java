package com.denis_kurmashev;

public class User {
  protected String name = "";
  protected String age = "0";

  public User(String name, String age) {
    this.name = name;
    this.age = age;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public String getAge() {
    return this.age;
  }

  public String toString() {
    return "name = " + this.name + "; \n age = " + this.age;
  }

}