<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %> 
<html>  
  <head>
    <title>Denis Kurmashev Web Application</title>
  </head>
  <body>
  
    <%
    
      HttpSession userSession = request.getSession();
      String sessionId = userSession.getId();

      com.denis_kurmashev.User user = (com.denis_kurmashev.User)userSession.getAttribute(sessionId);

    %>

    <h1>Welcome <%= user.getName() %></h1>
    <h3>You are <%= user.getAge() %> years old!</h3>

    <button id="logOffButton">Log out</button>

    <script>
      document.addEventListener('click', () => {

        fetch("/", { method: 'put' })
          .then(res => res.text())
          .then(res => console.log(res))
          .catch(ex => console.log(ex))
         

      }, true)
    </script>
  
  </body>
</html>
