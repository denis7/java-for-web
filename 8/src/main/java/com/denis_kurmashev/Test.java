package com.denis_kurmashev;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class Test extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void init() throws ServletException {
        super.init();

        ServletContext sc = getServletContext();
        ArrayList<User> users = new ArrayList<User>();

        sc.setAttribute("users", users);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {

        ServletContext sc = getServletContext();
        ArrayList<User> users = (ArrayList<User>) sc.getAttribute("users");

        String userId = request.getHeader("Authorazation");

        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == userId) {
                users.remove(i);
                sc.setAttribute("users", users);
                response.getWriter().write("Success");
                return;

            }
        }

        response.getWriter().write("Failure");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletContext sc = getServletContext();
        ArrayList<User> users = (ArrayList<User>) sc.getAttribute("users");

        String size = Integer.toString(users.size());

        String name = "NAME_" + size;
        String age = "AGE_" + size;

        User newUser = new User(name, age);

        users.add(newUser);

        sc.setAttribute("users", users);

        response.setHeader("Authorazation", newUser.getId());
        response.getWriter().write(newUser.getId());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletContext sc = getServletContext();
        ArrayList<User> users = (ArrayList<User>) sc.getAttribute("users");

        String userId = request.getHeader("Authorazation");

        for (int i = 0; i < users.size(); i++) {

            if (Objects.equals(users.get(i).getId().toString(), userId.toString())) {
                response.getWriter().write(users.get(i).toJSON());
                return;
            }

        }

        response.setStatus(401);
        response.getWriter().write("{\"error\": \"There is no 'Authorazation' token in request headers!\" }");

    }
}
