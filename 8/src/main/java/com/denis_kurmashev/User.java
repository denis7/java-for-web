package com.denis_kurmashev;

public class User {
  private static int index = 0;

  protected String id = "";
  protected String name = "";
  protected String age = "0";

  protected String genearateId() {
    return "hash_" + Integer.toString(index);
  }

  public User(String name, String age) {
    this.id = this.genearateId();
    this.name = name;
    this.age = age;
    index++;
  }

  public String getId() {
    return this.id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public String getAge() {
    return this.age;
  }

  public String toString() {
    return "name = " + this.name + "; \n age = " + this.age;
  }

  public String toJSON() {
    return "{ \"id\":\"" + this.id + "\", \"name\":\"" + this.name + "\", \"age\":\"" + this.age + "\"}";
  }

}