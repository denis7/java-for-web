<%@ page contentType="text/html;charset=UTF-8" language="java"
isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
  </head>
  <body>
    <script>
      fetch("http://localhost:8080/test", { method: "post" })
        .then(res => res.text())
        .then(token =>
          fetch("http://localhost:8080/test", {
            method: "get",
            headers: { Authorazation: token.toString() }
          })
            .then(res => res.json())
            .then(user => console.log(user))
            .catch(ex => console.log(ex))
        )
        .catch(ex => console.log(ex));
    </script>
  </body>
</html>
