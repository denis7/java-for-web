package com.denis_kurmashev;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;

public class AgeMiddleware implements Filter {
  public void init(FilterConfig cfg) throws ServletException {
    // during server loading
    System.out.println("AgeMiddleware:init");
  }

  public void destroy() {
    // before server will be stopped
    System.out.println("AgeMiddleware:destroy");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    RequestWithUser frq = new RequestWithUser((HttpServletRequest) request);

    frq.SetAge("19");

    chain.doFilter(frq, response);
  }
}
