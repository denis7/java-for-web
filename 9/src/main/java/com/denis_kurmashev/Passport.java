package com.denis_kurmashev;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;

public class Passport implements Filter {

  public void init(FilterConfig cfg) throws ServletException {
    // during server loading
    System.out.println("Passport:init");
  }

  public void destroy() {
    // before server will be stopped
    System.out.println("Passport:destroy");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    System.out.println("PASSPORT FILTER");

    String name = request.getParameter("name");
    String pass = request.getParameter("pass");

    if (name == null || pass == null) {
      response.getWriter().write("Data not provided");
      return;
    }

    if (!name.equals("Denis") || !pass.equals("Denis")) {
      response.getWriter().write("Incorrect name or password");
      return;
    }

    RequestWithUser frq = new RequestWithUser((HttpServletRequest) request);

    frq.SetName("Denis");
    frq.SetPassword("Denis");

    chain.doFilter(frq, response);
  }
}
