package com.denis_kurmashev;

import javax.servlet.http.*;

public class RequestWithUser extends HttpServletRequestWrapper implements HttpServletRequest {
  protected String name = null;
  protected String password = null;
  protected String age = null;

  public RequestWithUser(HttpServletRequest request) {
    super(request);

    if (request.getClass().getName().equals("com.denis_kurmashev.RequestWithUser")) {
      this.name = ((RequestWithUser) request).GetName();
      this.password = ((RequestWithUser) request).GetPassword();
      this.age = ((RequestWithUser) request).GetAge();
    }
  }

  public RequestWithUser(RequestWithUser request) {
    super((HttpServletRequest) request);

    this.name = request.GetName();
    this.password = request.GetPassword();
    this.age = request.GetAge();
  }

  public void SetName(String name) {
    this.name = name;
  }

  public String GetName() {
    return this.name;
  }

  public void SetPassword(String password) {
    this.password = password;
  }

  public String GetPassword() {
    return this.password;
  }

  public void SetAge(String age) {
    this.age = age;
  }

  public String GetAge() {
    return this.age;
  }
}