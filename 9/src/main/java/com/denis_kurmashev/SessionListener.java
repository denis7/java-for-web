package com.denis_kurmashev;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

  public void sessionCreated(HttpSessionEvent se) {
    HttpSession ss = se.getSession();
    ss.setMaxInactiveInterval(10);
  }

  public void sessionDestroyed(HttpSessionEvent se) {
    System.out.println("SessionListener:sessionDestroyed:Id=" + se.getSession().getId());
  }
}