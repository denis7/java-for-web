<%@ page contentType="text/html;charset=UTF-8" language="java"
isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
  </head>
  <body>
    
    <h1>Secured page</h1>
    <h4>User name: <%= ((com.denis_kurmashev.RequestWithUser) request).GetName() %> </h4>
    <h4>User age: <%= ((com.denis_kurmashev.RequestWithUser) request).GetAge() %> </h4>
    
  </body>
</html>
